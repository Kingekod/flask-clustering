from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired

class ShowForm(FlaskForm):
    first = StringField('first')
    second = StringField('second')
    submit = SubmitField('Show')
	
class MainForm(FlaskForm):
	username = StringField('Username', validators=[DataRequired()])
	submit = SubmitField('Submit')