from sklearn.cluster import KMeans
import pandas as pd
import matplotlib.pyplot as plt
import os

class Clustering:
	
	def __init__(self):
		self.dir = os.path.dirname(__file__)

	def remove_data(self):
		images = self.dir + '\static\images'
		if os.path.exists(images + '\mygraph1.png') and os.path.exists(images + '\mygraph2.png'):
			print(os.path.abspath(images + '\mygraph1.png'))
			os.remove(images + '\mygraph1.png')
			os.remove(images + '\mygraph2.png')
	
	def get_df(self, file='test.xlsx'):
		return pd.read_excel(file)
	
	def clustering(self, count_clusters=4):
		df = self.get_df()
		#self.df = pd.read_excel('test.xlsx')
		df = pd.read_excel('test.xlsx')
		del df['Персона']
		df.rename(columns={'Возраст, лет': 'Возраст', 
							'Стаж вождения, лет': 'Стаж вождения', 
							'Убыточность, %': 'Убыточность', 
							'Уровень заработной платы, руб/год': 'Уровень заработной платы'}, 
							inplace=True)
		kmeans = KMeans(n_clusters=count_clusters, random_state=0, init='k-means++', n_init=10)
		kmeans.fit_predict(df)
		return df, kmeans
		
	def get_graphs(self, first_input, second_input):
		df, kmeans = self.clustering()
		plt.clf()
		figure = plt.figure()
		axes = figure.gca()
		axes.set_xlabel(first_input)
		axes.set_ylabel(second_input) 
		axes.scatter(df[first_input], df[second_input])
		figure.savefig('app\static\images\mygraph1.png')
		image1 = self.dir+'\static\images\mygraph1.png'
		labels = kmeans.labels_
		plt.clf()
		figure = plt.figure()
		axes = figure.gca()
		axes.set_xlabel(first_input)
		axes.set_ylabel(second_input) 
		axes.scatter(df[first_input], df[second_input], c=labels)
		figure.savefig('app\static\images\mygraph2.png')
		image2 = self.dir+'\static\images\mygraph2.png'
		return image1, image2