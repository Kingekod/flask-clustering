from app import app
from flask import render_template, flash, redirect
from app.forms import ShowForm, MainForm

from app.clustering import Clustering

@app.route('/', methods=['GET','POST'])
def main():
	form = MainForm()
	if form.validate_on_submit():
		username = form.username.data
		return render_template('index.html', username=username)
	return render_template("main.html",
        title = 'Welcome',
        form = form)
	

@app.route('/index')
def index():
    user = { 'nickname': 'Artem' }
    return render_template("index.html",
        title = 'Home',
        user = user)
		
@app.route('/show', methods=['GET','POST'])
def show():
    form = ShowForm()
    clustering = Clustering()
    clustering.remove_data()
    if form.validate_on_submit():
        clustering.clustering()
        first = form.first.data
        second = form.second.data
        if first and second:
            flash('Кластеризация данных методом k-means')
            image1, image2 = clustering.get_graphs(first, second)
            return render_template('show.html', title='Show', form=form, image1=image1, image2=image2)
        else:
            flash('Поля для заполнения остались пустыми, введите необходимые данные')
            return render_template('show.html', title='Show', form=form)
    return render_template('show.html', title='Show', form=form)
#app.make_response()